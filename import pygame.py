import pygame
import gameH
import time
import tkinter as tk
from tkinter.messagebox import *
pygame.init()
win = pygame.display.set_mode((800, 800))
pygame.display.set_caption('Game')
waittime = 0.09
vel = 100
bg = (234, 181, 67)
selector = (0, 0, 0)
tilecolor1 = (186, 220, 88)
tilecolor2 = (106, 176, 76)
RED = (255, 0 , 0)
BLACK = (0, 0, 0)

class coord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def bound(self):
        if self.x > 700:
            self.x -= 100
        if self.x < 0:
            self.x += 100
        if self.y < 0:
            self.y += 100
        if self.y > 700:
            self.y -= 100

def death_check():
    if c.x == c2.x and c.y == c2.y:
        gameH.end()


c = coord(0, 200)
c2 = coord(100, 200)
run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_UP]:
        c.y -= vel
        c.bound()
        time.sleep(waittime)
        death_check()
    if keys[pygame.K_DOWN]:
        c.y += vel
        c.bound()
        time.sleep(waittime)
        death_check()
    if keys[pygame.K_LEFT]:
        c.x -= vel
        c.bound()
        time.sleep(waittime)
        death_check()
    if keys[pygame.K_RIGHT]:
        c.x += vel
        c.bound()
        time.sleep(waittime)
        death_check()
    if keys[pygame.K_w]:
        c2.y -= vel
        c2.bound()
        time.sleep(waittime)
    if keys[pygame.K_s]:
        c2.y += vel
        c2.bound()
        time.sleep(waittime)
        death_check()
    if keys[pygame.K_d]:
        c2.x += vel
        c2.bound()
        time.sleep(waittime)
        death_check()
    if keys[pygame.K_a]:
        c2.x -= vel
        c2.bound()
        time.sleep(waittime)
        death_check()
    win.fill(bg)
    gameH.make_grid(win, tilecolor1, tilecolor2)
    pygame.draw.rect(win, BLACK, (c.x, c.y, 100, 100), border_radius=10)
    pygame.draw.rect(win, RED, (c2.x, c2.y, 100, 100), border_radius=10)
    pygame.display.update()
