import requests
import sys
import tkinter as tk

s1 = input("What is your house number?")
s2 = input("What is the first word of your street name?")
s3 = input("What is the second word of your street name?")
street = "{}+{}+{}".format(s1,s2,s3) 
city = "Naperville"
state = "Illinois"
zipcode = "60565"

geocode = requests.get("https://geocoding.geo.census.gov/geocoder/locations/address?street={}&city={}&state={}&zip={}&benchmark=4&format=json".format(street, city, state, zipcode))

#print(geocode.json()["result"]["addressMatches"])
coordinates = geocode.json()["result"]["addressMatches"][0]["coordinates"]

gridpoints = requests.get("https://api.weather.gov/points/{},{}".format(coordinates["y"],coordinates["x"]))

forecast = requests.get(gridpoints.json()["properties"]["forecast"])
for days in forecast.json()["properties"]["periods"]:
    print("{}:\n{}{}, {}".format(days["name"],days["temperature"],days["temperatureUnit"],days["shortForecast"]))


#for keys in gridpoints.json()["properties"]["forecast"]:
 #   print(keys)
#print(geocode)
#for keys in geocode.json()["result"]["addressMatches"][0]["coordinates"]["x"]:
 #   print(keys)
#print(geocode.json())