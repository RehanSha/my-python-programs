import tkinter as tk
from tkinter import filedialog
from PIL import Image,ImageTk
root = tk.Tk()
def opn():
    a = filedialog.askopenfilename(filetypes=(('PNG Files','*.png'),('JPEG Files','*.jpg')))
    label = tk.Label(root,text=a).pack()
    img = ImageTk.PhotoImage(a)
    img_label = tk.Label(root,text=img).pack()
open_btn = tk.Button(root,text = 'Open Image',command=opn)
open_btn.pack()