from pip._vendor import requests
import tkinter as tk

#street = input('Enter your house address')
street = "23691+NE+15+Place"
#city = input('Enter your city name(Washington(No DC), Naperville, Columbia, etc.)')
city = "Sammamish"
#state = input('Enter your state name with no abbreviations.')
state = "Washington"
#zipcode = input('Enter your zipcode.')
zipcode = "98074"

geocode = requests.get("https://geocoding.geo.census.gov/geocoder/locations/address?street={}&city={}&state={}&zip={}&benchmark=4&format=json".format(street, city, state, zipcode))

#print(geocode.json()["result"]["addressMatches"])
coordinates = geocode.json()["result"]["addressMatches"][0]["coordinates"]

gridpoints = requests.get("https://api.weather.gov/points/{},{}".format(coordinates["y"],coordinates["x"]))

forecast = requests.get(gridpoints.json()["properties"]["forecast"])

class Application(tk.Frame):
    def __init__(self, master = None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.createWidgets()


    def createWidgets(self):
        for days in forecast.json()["properties"]["periods"]:
            self.days = days
            self.button = tk.Button(text=days["name"], command = lambda days=days: self.showWeather(days))
            self.button.pack(side ="top")

    def showWeather(self,days):
        print("\n{}{}, {}".format(days["temperature"],days["temperatureUnit"],days["shortForecast"]))
root = tk.Tk()
app = Application(master = root)
app.mainloop()