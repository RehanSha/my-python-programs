import turtle
from random import randint

t = turtle.Turtle()
window = turtle.Screen()
t.speed(0)
window.screensize(800, 800)

colorList = ["#c0392b", "#e74c3c","#d35400","#e67e22","#f39c12","#f1c40f","#2ecc71", "#27ae60", "#1abc9c", "#16a085", "#3498db","#2980b9","#9b59b6", "#8e44ad", "#d63031","#ff7675","#e17055","#fab1a0","#fdcb6e","#ffeaa7","#00b894","#55efc4","#00cec9","#81ecec","#0984e3","#74b9ff","#6c5ce7","#a29bfe","#e84393","#fd79a8","#b8e994","#78e08f","#38ada9","#079992","#fad390","#f6b93b","#fa983a","#f8c291","#e55039","#eb2f06","#b71540","#6a89cc","#4a69bd","#82ccdd","#cd84f1","#c56cf0","#32ff7e","#3ae374","#ffcccc","#ffb8b8","#7efff5","#67e6dc","#ff4d4d","#ff3838","#18dcff","#17c0eb","#ffaf40","#ff9f1a","#7d5fff","#7158e2","#fffa65","#fff200"]
colorIndex = 0
print(colorList.__sizeof__)

numbersUsed = []
step = 0
global currentNum
currentNum = 0
numbersUsed.append(step)
step += 1

def makeArt():
    t.begin_fill()
    t.circle(currentNum)
    t.end_fill()
    t.penup()
    t.right(randint(10, 185))
    t.forward(randint(75, 125))
    if t.xcor() >= 750 or t.xcor() <= -750:
        t.goto(randint(-550, 550), randint(-400, 400))
    if t.ycor() >= 750 or t.ycor() <= -750:
        t.goto(randint(-550, 550), randint(-400, 400))
    t.pendown()


for loop in range(100):
    
    print(loop)

    numbersUsed = []
    step = 0
    currentNum = 0
    numbersUsed.append(step)
    step += 1

    for i in range (20):
        back = currentNum - step
        if back > 0 and currentNum - step not in numbersUsed:
            currentNum -= step
        else:
            currentNum += step
        numbersUsed.append(currentNum)
        step += 1
        t.pencolor(colorList[colorIndex])
        t.fillcolor(colorList[colorIndex])
        if colorIndex >= 61:
            colorIndex = 0
        else:
            colorIndex += 1
        makeArt()
        

window.exitonclick()