import pygame
import time
from tkinter import filedialog
pygame.init()
win = pygame.display.set_mode((800, 800))
icon = pygame.image.load(filedialog.askopenfilename(filetypes=(('JPEG Files', '*.jpg'),('All Files', '*.*'))))
pygame.display.set_icon(icon)
pygame.display.set_caption('Game')
waittime = 0.5
vel = 100
bg = (234, 181, 67)
selector = (0, 0, 0)
selector2 = bg
tilecolor1 = (186, 220, 88)
tilecolor2 = (106, 176, 76)


class coord:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def bound(self):
        if self.x > 800:
            self.x -= 100
        if self.x < 0:
            self.x += 100
        if self.y < 200:
            self.y += 100
        if self.y > 500:
            self.y -= 100


def make_grid():
    pygame.draw.rect(win, tilecolor1, (0, 200, 100, 100))
    pygame.draw.rect(win, tilecolor1, (200, 200, 100, 100))
    pygame.draw.rect(win, tilecolor1, (400, 200, 100, 100))
    pygame.draw.rect(win, tilecolor1, (600, 200, 100, 100))
    pygame.draw.rect(win, tilecolor1, (800, 200, 100, 100))

    pygame.draw.rect(win, tilecolor2, (100, 200, 100, 100))
    pygame.draw.rect(win, tilecolor2, (300, 200, 100, 100))
    pygame.draw.rect(win, tilecolor2, (500, 200, 100, 100))
    pygame.draw.rect(win, tilecolor2, (700, 200, 100, 100))
    pygame.draw.rect(win, tilecolor2, (900, 200, 100, 100))

    pygame.draw.rect(win, tilecolor2, (0, 300, 100, 100))
    pygame.draw.rect(win, tilecolor2, (200, 300, 100, 100))
    pygame.draw.rect(win, tilecolor2, (400, 300, 100, 100))
    pygame.draw.rect(win, tilecolor2, (600, 300, 100, 100))
    pygame.draw.rect(win, tilecolor2, (800, 300, 100, 100))

    pygame.draw.rect(win, tilecolor1, (100, 300, 100, 100))
    pygame.draw.rect(win, tilecolor1, (300, 300, 100, 100))
    pygame.draw.rect(win, tilecolor1, (500, 300, 100, 100))
    pygame.draw.rect(win, tilecolor1, (700, 300, 100, 100))
    pygame.draw.rect(win, tilecolor1, (900, 300, 100, 100))

    pygame.draw.rect(win, tilecolor1, (0, 400, 100, 100))
    pygame.draw.rect(win, tilecolor1, (200, 400, 100, 100))
    pygame.draw.rect(win, tilecolor1, (400, 400, 100, 100))
    pygame.draw.rect(win, tilecolor1, (600, 400, 100, 100))
    pygame.draw.rect(win, tilecolor1, (800, 400, 100, 100))

    pygame.draw.rect(win, tilecolor2, (100, 400, 100, 100))
    pygame.draw.rect(win, tilecolor2, (300, 400, 100, 100))
    pygame.draw.rect(win, tilecolor2, (500, 400, 100, 100))
    pygame.draw.rect(win, tilecolor2, (700, 400, 100, 100))
    pygame.draw.rect(win, tilecolor2, (900, 400, 100, 100))

    pygame.draw.rect(win, tilecolor2, (0, 500, 100, 100))
    pygame.draw.rect(win, tilecolor2, (200, 500, 100, 100))
    pygame.draw.rect(win, tilecolor2, (400, 500, 100, 100))
    pygame.draw.rect(win, tilecolor2, (600, 500, 100, 100))
    pygame.draw.rect(win, tilecolor2, (800, 500, 100, 100))

    pygame.draw.rect(win, tilecolor1, (100, 500, 100, 100))
    pygame.draw.rect(win, tilecolor1, (300, 500, 100, 100))
    pygame.draw.rect(win, tilecolor1, (500, 500, 100, 100))
    pygame.draw.rect(win, tilecolor1, (700, 500, 100, 100))
    pygame.draw.rect(win, tilecolor1, (900, 500, 100, 100))


c = coord(0, 200)
run = True
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_UP]:
        c.y -= vel
        c.bound()
        # empty line
        time.sleep(waittime)
    if keys[pygame.K_DOWN]:
        c.y += vel
        c.bound()
        # empty line
        time.sleep(waittime)
    if keys[pygame.K_LEFT]:
        c.x -= vel
        c.bound()
        # empty line
        time.sleep(waittime)
    if keys[pygame.K_RIGHT]:
        c.x += vel
        c.bound()
        # empty line
        time.sleep(waittime)
    win.fill(bg)
    make_grid()
    pygame.draw.rect(win, selector, (c.x, c.y, 100, 100), border_radius=10)
    pygame.draw.rect(win, selector2, (c.x + 10, c.y + 10, 80, 80))
    pygame.display.update()
